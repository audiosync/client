# Build application
FROM node:alpine as build-stage

WORKDIR /app

# Install package as a separate layer for improved docker caching
COPY package*.json ./
RUN npm install

# Build the application
COPY . .
RUN npm run build


# Runnable image
FROM tinou98/vue-12factor

COPY --from=build-stage /app/dist /srv/http
CMD ["js/*.js", "index.html"] # List file that contain run-time variable
