import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const hist = 10;

import KalmanFilter from 'kalmanjs';
const kf = new KalmanFilter();

/**
 * Return now formated as a stirng
 * @return {String}
 */
function nowStr() {
  const date = new Date;
  return date.getHours() +
    ':' + date.getMinutes() +
    ':' + date.getSeconds();
}

export default new Vuex.Store({
  state: {
    value: null,
    lastValue: null,
    labels: [...new Array(hist)].map((_, i) => `${i}`),
    values: [...new Array(hist)].map(() => [0, 0]),

    searchResult: [],
    videoId: null,
    proposal: null,
  },
  mutations: {
    doProposal(state, accept) {
      if (accept) {
        state.videoId = state.proposal.videoUrl;
      }
      state.proposal = null;
    },
    pong(state, interval) {
      state.lastValue = interval;
      state.value = kf.filter(interval);

      state.labels.push(nowStr());
      state.labels = state.labels.slice(-hist);

      state.values.push([interval, state.value - interval]);
      state.values = state.values.slice(-hist);
    },
    SOCKET_search(state, videos) {
      state.searchResult = videos;
      console.log(state.searchResult);
    },
    SOCKET_setVideo(state, prop) {
      if (prop.videoUrl !== state.videoId) {
        state.proposal = prop;
      }
    },
  },
});
